from flask import Flask, render_template, flash, request,jsonify
import re
import html
import sys
import math
from datetime import date
import requests
import json
import mwparserfromhell
import config

class TaxoboxalyzerError(Exception):
    """Exception raised for logical exceptions in this module

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message, category=None):
        self.message = message
        self.category = category

# The parameters should be removed when converting to automatic taxobox
taxoparams = ["superdomain", "unranked_superdomain","domain", "unranked_regnum", "superregnum", "regnum", "subregnum",
              "unranked_superphylum", "unranked_phylum", "superdivisio", "superphylum", "phylum", "subphylum", "infraphylum", "microphylum", "nanophylum",
              "unranked_classis", "superclassis", "classis", "subclassis", "infraclassis", "unranked_infraclassis",
              "unranked_ordo", "magnordo", "superordo", "ordo", "subordo", "infraordo", "parvordo", 
              "unranked_superfamilia", "superfamilia", "familia", "subfamilia", 
              "zoodivisio", "zoosectio", "zoosubsectio",
              "unranked_tribus", "supertribus", "tribus", "subtribus", "sectio",             
              "unranked_divisio", "divisio", "subdivisio",
              "color", ]

# remove unwanted italic and bolding markup from parameters, and multiple spaces
def strreg(string):
    retval = re.sub(r"'", "", str(string))
    retval = re.sub(r"[ ]+", " ", retval)
    return retval

paramDict = {   "accessdate" : "access-date",
                "archivedate": "archive-date",
                "archiveurl": "archive-url",
                "authorlink": "author-link", 
                "author1link": "author-link1", 
                "author2link": "author-link2", 
                "author3link": "author-link3", 
                "author4link": "author-link4", 
                "author5link": "author-link5", 
                "author6link": "author-link6",                                                                                                
                "author7link": "author-link7", 
                "author8link": "author-link8", 
                "author9link": "author-link9"                                                                                                           
                }

def paramFix(mwpfh, edit_summary):
    pfixed = False
    for template in mwpfh.filter_templates():
        if template.name != "Cite archival metadata":
            for param in template.params:
                paramname = str(param.name).strip() 
                if paramname in paramDict:
                    param.name=paramDict[paramname]
                    pfixed = True
    if pfixed:
        edit_summary += "; param fix"
    return edit_summary

app = Flask(__name__)
app.config.from_object(config)
app.config.from_pyfile('local.cfg', silent=True)

# index page. Could be a static file, really.
@app.route('/')
def index():
    return render_template('Index.html.jinja')

# Process a page with a taxobox
@app.route("/form/<title>")
def ctrl_html(title):
    return ctrl_form(title, "html")

@app.route("/json/<title>", methods=['POST', 'GET'])
def ctrl_json(title):
    return ctrl_form(title, "json")

def ctrl_form(title, reqtype):
    paramfix  = request.args.get('params', default = True, type = bool)
    boxconv  = request.args.get('box', default = True, type = bool)
    edit_summary = ''
    regnum = None
    plantflag = False
    oldbox = None
    italictitle = None
    mwpfh = None
    image = ''
    imagewidth = ''
    image2 = ''
    image2width = ''
    genusparam = None
    genuslink = None
    binomial = title.replace('_', ' ') + '\n'
    binomialparamname = 'species'
    speciesparam = None
    edit_summary = request.form.get('wpSummary')
    if not edit_summary:
        edit_summary = ""
    report_err = ""
    page = None
    pgtext = request.form.get('wpTextbox1')
    if not pgtext:
        page = getPage(title)
        if page:
            pgtext = page["revisions"][0]["*"]  
        else: 
            raise TaxoboxalyzerError('no page text found for' + title, 'error')
    mwpfh = mwparserfromhell.parse(pgtext, skip_style_tags=True)
    try:
        if boxconv:
            for template in mwpfh.filter_templates():
                if template.name.matches("Taxobox"):
                    oldbox = template
                if template.name.matches(['Italic title', 'Italictitle']):
                    italictitle = template

        if oldbox:
            if oldbox.has('trinomial'): raise TaxoboxalyzerError('taxobox has trinomial', 'error')

            if oldbox.has('image'):
                image = strreg((oldbox.get('image')).value).strip()
            if oldbox.has('image_width'):
                imagewidth = strreg((oldbox.get('image_width')).value).strip()

            if oldbox.has('image2'):
                image = strreg((oldbox.get('image2')).value).strip()
            if oldbox.has('image2_width'):
                imagewidth = strreg((oldbox.get('image2_width')).value).strip()

            if oldbox.has('genus'):
                genusparam = oldbox.get('genus')
                genuslink = strreg(genusparam.value).strip()
            else:
                raise TaxoboxalyzerError('No genus found', 'error')

            if oldbox.has('binomial') or oldbox.has('species'):
                if oldbox.has('regnum'):
                    regnum_param = oldbox.get('regnum')
                    regnum = str(regnum_param.value)
                    if re.search(r'[Pp]lant', regnum):
                        plantflag = True
                if oldbox.has('binomial'):
                    binomialparamname = 'binomial'
                    binomial_param = oldbox.get('binomial')
                    binomial = strreg(binomial_param.value.nodes[0])
                else:
                    binomial_param = oldbox.get('species')

            if oldbox.has('binomial') or oldbox.has('species'):
                print(f"Processing {binomial}")
                binomial_parts = binomial.split()
                if len(binomial_parts) == 2:

                        links = genusparam.value.filter_wikilinks()
                        if links:
                            genuslink = str(links[0].title)
                        else:
                            flash(
                                f"Genus parameter did not have a link. Assuming page is at {genuslink}")
                        genus_taxo = None
                        if genuslink != title:
                            genuspg = getPage(genuslink)
                            genustext = genuspg["revisions"][0]["*"] if genuspg else ''
                            genus_mwpfh = mwparserfromhell.parse(genustext, skip_style_tags=True)
                            abox = genus_mwpfh.filter_templates(
                                matches=r'[Aa]utomatic[ _]taxobox')
                            if len(abox) > 0:
                                genus_taxo = abox[0].get('taxon') if abox[0].has('taxon') else None
                            else:
                                flash(
                                    'Genus page has manual taxonbox. Cannot determine correct taxon template for genus.')

                        oldbox.name.replace(oldbox.name.strip(), "Speciesbox")
                        if italictitle:
                            mwpfh.remove(italictitle)
                            flash(
                                'Italic title template removed: not needed with automatic taxobox')
                        if oldbox.has('binomial_authority'):
                            authparam = oldbox.get('binomial_authority')
                            authparam.name.replace(
                                'binomial_authority', 'authority')
                        if oldbox.has('genus_authority'):
                            authparam = oldbox.get('genus_authority')
                            authparam.name.replace(
                                'genus_authority', 'parent_authority')
                            oldbox.add('display_parents', '2', before='genus')
                        genustaxostr = str(genus_taxo.value).strip() if genus_taxo else genuslink
                        if plantflag or genustaxostr != binomial_parts[0]:
                            genusparam.value.replace(
                            # If there is no taxon param in the template on the genus page, it's using the pagename
                            str(genusparam.value).strip(),genustaxostr )
                            species_taxo = oldbox.get('species')
                            species_taxo.value.replace(
                                    species_taxo.value.strip(), binomial_parts[1])
                            oldbox.remove('binomial')
                        else:
                            binomial_param.value = binomial
                            binomial_param.name.replace(binomialparamname , 'taxon')
                            if (binomialparamname != 'species'):
                                oldbox.remove('species')
                            oldbox.remove('genus')
            else:
                flash(f"Converting page for a genus. Assuming taxonomy is at {genuslink.strip()}")
                genusparam.name.replace('genus', 'taxon')
                genusparam.value = re.sub(r"'*", "", str(genusparam.value))
                if oldbox.has('genus_authority'):
                    authparam = oldbox.get('genus_authority')
                    authparam.name.replace(
                                'genus_authority', 'authority')
                oldbox.name.replace(oldbox.name.strip(), "Automatic_taxobox")

            for pkey in taxoparams:
                if oldbox.has(pkey):
                    oldbox.remove(pkey)
            if oldbox.has("color"):
                oldbox.remove("color")

            handleWidth(oldbox, image, imagewidth, 'image_width','image_upright')
            handleWidth(oldbox, image2, image2width, 'image2_width','image2_upright')

            if oldbox.has('subgenus'):
                oldbox.remove('subgenus')
                flash( f"removed subgenus parameter - parent parameter may be required")

            if oldbox.has('name') and binomial:
                binomwikitext = mwparserfromhell.parse(binomial.strip())
                if binomwikitext.matches(title.replace('_',' ')) and binomwikitext.matches(strreg(oldbox.get('name').value)):
                    oldbox.remove('name')
                    flash( f"removed superfluous name parameter")

            edit_summary = f"Change Taxobox to { oldbox.name }" 

        edit_summary = paramFix(mwpfh, edit_summary)

    except TaxoboxalyzerError as err:
        flash(err.message, err.category)
        report_err = err.message
    if reqtype=="html":
        return render_template('EditBox.html.jinja', boxtext=html.escape(str(oldbox)), newtext=html.escape(str(mwpfh)), title=title, edit_summary = edit_summary)
    else:
        return jsonify(boxtext=str(oldbox), newtext=str(mwpfh), title=title, edit_summary = edit_summary, error=report_err)

def handleWidth(oldbox, image, imagewidth, paramname, newparamname):
    if oldbox.has(paramname):
        if (not image or not imagewidth):
            oldbox.remove(paramname)
            flash( f"removed superfluous image_width parameter")
        else:
            newimagewidth = imagewidth.replace('px', '')
            imagewidthnumeric = -1
            if newimagewidth.isnumeric():
                 imagewidthnumeric = int(newimagewidth)
            if (imagewidthnumeric > 0):
                i_up = imagewidthnumeric * 100.0 / 220
                if (math.fabs(i_up - 100.0) > 4.5):
                    iparam = oldbox.get(paramname)
                    iparam.name.replace(paramname, newparamname)
                    iparam.value.replace(imagewidth, str(round(i_up / 100, 2)))
                    flash( f"converted image_width parameter")
                else:
                    oldbox.remove(paramname)
                    flash( f"removed pointless image_width parameter")

def getPage(title):
    with requests.get(url=app.config["API_PATH"],
                      params={
                      'action': 'query',
                      'prop': 'revisions',
                      'format': 'json',
                      'rvprop': 'timestamp|content',
                      'titles': title,
                      'redirects': 'true'
                      },
                      headers = {
                           'User-Agent': app.config["USER_AGENT"]
                      }) as resp:
         if (resp.status_code == 200):
            jObj = json.loads(resp.content)
            if jObj["query"] and jObj["query"]["pages"]:
               for  id in jObj["query"]["pages"].keys():
                  return None if id == '-1' else jObj["query"]["pages"][id]
         else:
            print(f"HTTP response: {resp.status_code}")
         return None

@app.after_request
def set_access(response):
    response.headers['Access-Control-Allow-Origin']='https://en.wikipedia.org'
    response.headers['Access-Control-Allow-Methods']='GET, POST'
    return response
