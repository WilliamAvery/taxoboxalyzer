import os

SECRET_KEY = os.urandom(12).hex()
USER_AGENT = "Taxoboxalyzer 0.1"
API_PATH = "https://en.wikipedia.org/w/api.php"
EDIT_SUMMARY = "using [[User:William Avery/taxoboxalyzer|Taxoboxalyzer]]"